package com.goddy.fluximport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FluxImportApplication {

  public static void main(String[] args) {
    SpringApplication.run(FluxImportApplication.class, args);
  }
}
